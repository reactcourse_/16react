import jwt from 'jsonwebtoken'

export const userMidleware = (req, res, next)=>{
    let token = req.headers['authorization']
    if(token)
    {
        try{
        token = token.replace('Bearer ', '')
        const decodedToken = jwt.verify(token, process.env.JWT_SECRET)
        req.user = decodedToken
        }
        catch(e){}
    }
    console.log(token)
   
    next()
}

export const authGuard = (req,res,next)=>{
    if(!req.user){
        res.status(403).send()
        return
    }
    next()
}