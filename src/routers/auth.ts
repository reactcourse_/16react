import { Router } from "express";
import bcrypt from "bcrypt";
import { checkRequiredMidleware } from "../midlewares/checkRequired.midleware";
import jwt from 'jsonwebtoken';
import { authGuard } from "../midlewares/user.midlewae";

export const authRouter = Router();

const users:any[] = [];

authRouter.post("/register", checkRequiredMidleware(['email', 'password']), async (req, res) => {
  console.log(req.body);

  const { email, password } = req.body;
  const hash = await bcrypt.hash(password,Number(process.env.SALT_ROUNDS))
  users.push({
    email,
    hash,
  });
  res.send(`ok ${email}`);
});

authRouter.post("/login",checkRequiredMidleware(['email', 'password']), async (req, res) => {
    const { email, password } = req.body;
    const user = users.find(user => user.email === email)
    if(user){
        const passwordCorrect = await bcrypt.compare(password, user.hash)
        if(passwordCorrect)
        {
            var token = jwt.sign({foo: 'bar'},process.env.JWT_SECRET); 

            res.send({
                email,
                hash:user.hash,
                token
            })
        }
        return
    }
    res.status(400).send('Incorrect email or password')
});

authRouter.get('/users',authGuard, (req,res)=>{
    // @ts-ignore
    console.log(req.user)
    res.send(users)
})