import './src/utils/config'
import express from 'express'
import { router } from './src/routers'
import { errorHandlingMidleware } from './src/midlewares/errorHandling.midleware'
import cookieSession from 'cookie-session'
import { userMidleware } from './src/midlewares/user.midlewae'

const app = express()
app.use(cookieSession({secret: 'qwerty'}));
app.use((req,res,next)=>{
    if(req.session)
        req.session.count = (req.session.count || 0)+1
    console.log(req.session)
    next()
})
app.use(express.json())

app.use(userMidleware)
app.use(router)
app.use(errorHandlingMidleware)

const PORT = process.env.PORT || 4343

app.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`)
})